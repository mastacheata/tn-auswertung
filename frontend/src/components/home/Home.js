import React, { Component } from 'react'

class Home extends Component {

  render() {
    return (
      <div className="columns">
        <div className="column col-4 col-mx-auto">
          <h1 className="text-center">Bitte zuerst Daten importieren</h1>
        </div>
      </div>
    );
  }
}

export default Home;