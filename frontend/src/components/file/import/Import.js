import React, { Component } from 'react'
import CustomFilePond from './CustomFilePond';

class Import extends Component {

  render() {
    return (
      <div className="column col-6">
        <CustomFilePond className="mv-auto" ref={ref => this.pond = ref} />
      </div>
    );
  }
}

export default Import;