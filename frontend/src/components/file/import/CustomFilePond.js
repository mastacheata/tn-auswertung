import React, { Component } from 'react'
import { FilePond } from 'react-filepond'

import 'filepond/dist/filepond.min.css';

class CustomFilePond extends Component {

  constructor(props) {
    super(props);
    this.pond = React.createRef();
  }

  handleUpload(error, {file}) {
    console.log(this.props);
    console.log(file);
  }

  render() {
    return (
      <FilePond ref={this.pond} labelIdle={"Tanknetz Export hierhin ziehen oder <span class=\"filepond--label-action\"> Durchsuchen </span>"} onaddfile={this.handleUpload}/>
    );
  }
}

export default CustomFilePond;