import React, { Component } from 'react'

class Export extends Component {

  render() {
    return (
      <div className="column col-6">
        <a href="/" className="btn btn-primary centered mv-auto">Für Excel exportieren</a>
      </div>
    );
  }
}

export default Export;