import React, { Component } from 'react';

import Import from './import/Import';
import Export from './export/Export';

import './File.css';

class File extends Component {

  render() {
    return (
      <div className="columns">
        <Import />
        <Export />
      </div>
    );
  }
}

export default File;