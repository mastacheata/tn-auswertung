import React, { Component } from 'react'
import {ReactComponent as Logo} from '../../../gas.svg';
import { Link } from "@reach/router";

class Menu extends Component {

  render() {
    return (
      <header className="navbar">
        <section className="navbar-section">
          <div className="dropdown">
            <span className="btn btn-link dropdown-toggle" tabIndex="1">
              <Link to="/file">Import/Export</Link>
            </span>
          </div>
          <div className="dropdown">
            <span className="btn btn-link dropdown-toggle" tabIndex="2">
              <Link to="/config">Konfiguration laden/speichern</Link>
            </span>
          </div>
          <div className="dropdown">
            <span className="btn btn-link dropdown-toggle" tabIndex="3">
              <Link to="/config">Einträge bearbeiten</Link>
            </span>
          </div>
          <div className="dropdown">
            <span className="btn btn-link dropdown-toggle" tabIndex="4">
              <Link to="view">Ansicht</Link>
            </span>
          </div>
        </section>
        <section className="navbar-center">
          <Link to="/">
            <Logo style={{height: "3rem", width: "3rem"}} />
          </Link>
        </section>
        <section className="navbar-section">
          <button className="btn btn-action"><i className="icon icon-cross" /></button>
        </section>
      </header>
    );
  }
}

export default Menu;