import React, { Component } from 'react';
import { Router } from '@reach/router';
import { ApolloProvider } from "react-apollo";

import Menu from './components/common/menu/Menu'
import Home from './components/home/Home';
import File from './components/file/File';
import Config from './components/config/Config';
import AddonData from './components/addondata/AddonData';
import View from './components/view/View';
import Table from './components/table/Table';
import GraphQLClient from './lib/GraphQLClient'

import 'spectre.css';
import 'spectre.css/dist/spectre-icons.css';

class App extends Component {
  render() {
    return (
      <div className="container">
        <ApolloProvider client={GraphQLClient}>
          <Menu />
          <Router>
            <Home path="/" />
            <File path="/file" />
            <Config path="/config" />
            <AddonData path="/addon" />
            <View path="/view" />
          </Router>
          <Table />
        </ApolloProvider>
      </div>
    );
  }
}

export default App;
